// Write a function that returns true if the brackets in a given string are balanced.
// The function must handle parens (), square brackets [], and curly braces {}.

import java.io.*;
import java.util.*;

public class ParensMatch {
  public static void main (String[] args) {
    System.out.println("Test: (a[0]+b[2c[6]]) {24 + 53}  passed:" + parensMatch("(a[0]+b[2c[6]]) {24 + 53}"));
    System.out.println("Test: f(e(d))                    passed:" + parensMatch("f(e(d))"));
    System.out.println("Test: [()]{}([])                 passed:" + parensMatch("[()]{}([])"));
    System.out.println("Test: ((b)                       passed:" + parensMatch("((b)"));
    System.out.println("Test: (c]                        passed:" + parensMatch("(c]"));
    System.out.println("Test: {(a[])                     passed:" + parensMatch("{(a[])"));
    System.out.println("Test: ([)]                       passed:" + parensMatch("([)]"));
    System.out.println("Test: )(                         passed:" + parensMatch(")("));
    System.out.println("Test: empty                      passed:" + parensMatch(""));
	System.out.println("Test: {[()]}                     passed:" + parensMatch("{[()]}"));
    System.out.println("Test: {}[]()                     passed:" + parensMatch("{}[]()"));
    System.out.println("Test: a(p: string) {do{print} while(true)}                     passed:" + parensMatch("a(p: string) {do{print while(true)}"));
  }
  
  public static boolean parensMatch(String sentence){ 
    if(sentence != null && sentence.compareTo("")==0){
      return false;
    }
    Stack<Character> st = new Stack<Character>();
    char[] elements = sentence.toCharArray();
    boolean ret = true;
    
    for (int i = 0 ; i < elements.length ; i ++){
      char ele = elements[i];
      if(ele == '('){
        st.push(ele);
      }
      else if(ele == '{'){
        st.push(ele);
      }
      else if(ele == '['){
        st.push(ele);
      }
      else if(ele == ']'){
        if(st.isEmpty() || st.pop() != '['){
          return false;
        }
      }      
      else if(ele == '}'){
        if(st.isEmpty() || st.pop() != '{'){
          return false;
        }
      }      
      else if(ele == ')'){
        if(st.isEmpty() || st.pop() != '('){
          return false;
        }
      }
      
    }
    return st.isEmpty();
  }
  
}